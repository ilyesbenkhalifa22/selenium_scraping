%%shell
# Ubuntu no longer distributes chromium-browser outside of snap
#
# Proposed solution: https://askubuntu.com/questions/1204571/how-to-install-chromium-without-snap

# Add debian buster
cat > /etc/apt/sources.list.d/debian.list <<'EOF'
deb [arch=amd64 signed-by=/usr/share/keyrings/debian-buster.gpg] http://deb.debian.org/debian buster main
deb [arch=amd64 signed-by=/usr/share/keyrings/debian-buster-updates.gpg] http://deb.debian.org/debian buster-updates main
deb [arch=amd64 signed-by=/usr/share/keyrings/debian-security-buster.gpg] http://deb.debian.org/debian-security buster/updates main
EOF

# Add keys
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys DCC9EFBF77E11517
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 648ACFD622F3D138
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 112695A0E562B32A

apt-key export 77E11517 | gpg --dearmour -o /usr/share/keyrings/debian-buster.gpg
apt-key export 22F3D138 | gpg --dearmour -o /usr/share/keyrings/debian-buster-updates.gpg
apt-key export E562B32A | gpg --dearmour -o /usr/share/keyrings/debian-security-buster.gpg

# Prefer debian repo for chromium* packages only
# Note the double-blank lines between entries
cat > /etc/apt/preferences.d/chromium.pref << 'EOF'
Package: *
Pin: release a=eoan
Pin-Priority: 500


Package: *
Pin: origin "deb.debian.org"
Pin-Priority: 300


Package: chromium*
Pin: origin "deb.debian.org"
Pin-Priority: 700
EOF

# Install chromium and chromium-driver
apt-get update
apt-get install chromium chromium-driver

# Install selenium
pip install selenium



from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from bs4 import BeautifulSoup

url = "https://www.seloger.com" 
options = Options()
#run Selenium in headless mode
options.add_argument('--headless')
options.add_argument('--no-sandbox')
#overcome limited resource problems
options.add_argument('--disable-dev-shm-usage')
options.add_argument("lang=en")
#open Browser in maximized mode
options.add_argument("start-maximized")
#disable infobars
options.add_argument("disable-infobars")
#disable extension
options.add_argument("--disable-extensions")
options.add_argument("--incognito")
options.add_argument("--disable-blink-features=AutomationControlled")

#options.headless = True

driver = webdriver.Chrome(options=options)

driver.get(url)
# Get the page source and create a Beautiful Soup object
html_source = driver.page_source
soup = BeautifulSoup(html_source, 'html.parser')

# Print the entire HTML of the page
print(soup.prettify())



#slider captcha
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
import time, re
from lxml import etree
from urllib import request
from PIL import Image
import cv2

from matplotlib import pyplot as plt
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait


# OpenCV reference : https://zwindr.blogspot.com/2017/02/python-opencv-matchtemplate.html
def opencv():
    img = cv2.imread('captcha.png')
    img2 = img.copy()
    template = cv2.imread('pattern.png')
    w = template.shape[1]
    h = template.shape[0]

    # All the 6 methods for comparison in a list
    methods = ['cv2.TM_CCOEFF', 'cv2.TM_CCOEFF_NORMED', 'cv2.TM_CCORR',
               'cv2.TM_CCORR_NORMED', 'cv2.TM_SQDIFF', 'cv2.TM_SQDIFF_NORMED']
    methods = ['cv2.TM_CCOEFF']
    for meth in methods:
        img = img2.copy()
        method = eval(meth)

        # Apply template Matching
        res = cv2.matchTemplate(img, template, method)
        min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)

        # If the method is TM_SQDIFF or TM_SQDIFF_NORMED, take minimum
        if method in [cv2.TM_SQDIFF, cv2.TM_SQDIFF_NORMED]:
            top_left = min_loc
        else:
            top_left = max_loc
        bottom_right = (top_left[0] + w, top_left[1] + h)

        cv2.rectangle(img, top_left, bottom_right, 255, 2)
        print("top_left=",top_left)
        plt.subplot(121), plt.imshow(res, cmap='gray')
        plt.title('Matching Result'), plt.xticks([]), plt.yticks([])
        plt.subplot(122), plt.imshow(img)
        plt.title('Detected Point'), plt.xticks([]), plt.yticks([])
        plt.suptitle(meth)

        plt.show()
        break

    return top_left


options = Options()
#run Selenium in headless mode
options.add_argument('--headless')
options.add_argument('--no-sandbox')
#overcome limited resource problems
options.add_argument('--disable-dev-shm-usage')
options.add_argument("lang=en")
#open Browser in maximized mode
options.add_argument("start-maximized")
#disable infobars
options.add_argument("disable-infobars")
#disable extension
options.add_argument("--disable-extensions")
options.add_argument("--incognito")
options.add_argument("--disable-blink-features=AutomationControlled")

#options.headless = True

driver = webdriver.Chrome(options=options)

actions = ActionChains(driver)
driver.get('https://www.seloger.com/list.htm?tri=initial&enterprise=0&idtypebien=2,1&div=2238&idtt=2,5&naturebien=1,2,4&m=search_hp_new') #
time.sleep(2)


html1 = etree.HTML(driver.page_source)
URL = html1.xpath('//*[@id="captcha__frame"]')
print("URL=",URL)
time.sleep(1)

WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.XPATH,'//*[@id="captcha__element"]')))
time.sleep(5)
inputTag = driver.find_element_by_xpath('//*[@id="captcha__frame__bottom"]/div[2]/div[1]')
imgurTag = driver.find_element_by_xpath('//*[@id="captcha__puzzle"]/canvas[1]').get_attribute('src')  # mind

# https://stackoverflow.com/questions/28078812/get-iframe-src-via-xpath

print("imgurTag=",imgurTag )

#### need to remove "*"
imgurl2 = re.sub(r'\*','',imgurTag)
print("imgurl2=",imgurl2 )

request.urlretrieve(imgurl2, 'captcha.png')  # save image

position = opencv()
print(position)
newpos= ( position[0]- 76 ) * 0.4176  # the length iframe is 284 and the length opencv picture is  680   284/680  get convert ratio
print("newpos=",newpos)
inputTag = driver.find_element_by_xpath('//div[@id="tcaptcha_drag_thumb"]')

# #
action = ActionChains(driver)
webdriver.ActionChains(driver).drag_and_drop_by_offset(inputTag, newpos-1, 0).perform()
time.sleep(1)
webdriver.ActionChains(driver).drag_and_drop_by_offset(inputTag, 1, 0).perform()

time.sleep(2)
# #

print("END")
